﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Jogo
{
    class Cut
    {
        public Vector2 pos;
        public int life_spam;
        public int alive;
        public Cut(Vector2 p)
        {
            pos.X = p.X;
            pos.Y = p.Y;
            life_spam = 1000;
            alive = 1;
        }
        public void Update() 
        {
            if (alive == 0)
            {
                return;
            }
            life_spam--;
            if (life_spam < 0)
            {
                alive = 0;
            }
        }
        public void Draw()
        {
            //Todo 
            return;
        }
    }
    class Hole
    {
        public Vector2 pos;
        public int life_spam;
        public int alive;
        public void Update()
        {

        }
        public void Draw()
        {

        }
    }
    class HoleManager
    {
        List<Hole> holes;
        public void Update()
        {

        }
        public void Add()
        {

        }
        public void makeHole()
        {

        }
        public void Draw() 
        {

        }
    }
    class CutManager
    {
        List<Cut> cuts;
        List<List<int>> field;
        HoleManager hole_manager;
        public CutManager()
        {
            cuts = new List<Cut>();
        }
        public void Add(Cut c) 
        {
            cuts.Add(c);
            field[(int)c.pos.X][(int)c.pos.Y] = 1;
        }
        public void Update()
        {
            foreach (Cut c in cuts)
            {
                c.Update();
            }
            List<Vector2> pos = new List<Vector2>();
            pos.Add(game.player1.Position);
            pos.Add(game.player2.Position);
            
            foreach (Vector2 p in pos)
            {
                if (field[(int)p.X][(int)p.Y] == 1)
                {
                    hole_manager.makeHole();
                }
                else
                {
                    field[(int)p.X][(int)p.Y] = 1;
                    this.Add(new Cut(p));
                }
            }
        }
        public void Draw()
        {
            foreach (Cut c in cuts)
            {
                c.Draw();
            }
            hole_manager.Draw();
        }
    }
}
