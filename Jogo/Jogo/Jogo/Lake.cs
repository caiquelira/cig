﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Jogo
{
    class Lake
    {
        private static Viewport viewport;
        private static SpriteBatch spriteBatch;
        public static Texture2D texture;

        public static void Initialize(Viewport viewport, SpriteBatch spriteBatch)
        {
            Lake.viewport = viewport;
            Lake.spriteBatch = spriteBatch;
        }

        public static void Load(Texture2D texture)
        {
            Lake.texture = texture;
            int a = Lake.texture.Width;
        }

        public void Draw()
        {
            spriteBatch.Draw(
                texture,     // A textura a ser desenhada
                new Vector2 (0, 0),    // A posição (vetorial) na qual desenhar
                null,               // O retângulo na textura de origem a ser desenhado (null -> toda a textura)
                Color.White,     // O filtro de cor a ser aplicado sobre a textura (branco -> deixa todas as cores iguais)
                0,       // O ângulo do qual rotacionar a imagem
                new Vector2(texture.Width / 2, texture.Height / 2), // O ponto dentro da textura que vai ser desenhado no ponto indicado no segundo parâmetro
                1,                  // A escala da imagem
                SpriteEffects.None, // Efeito (reflexão horizontal/vertical)
                0.0f);                 // Profundidade. Se menor, vai ser desenhado em cima. (Não usem isto. Desnhem na ordem certa no código mesmo.)
        }
    }

}
