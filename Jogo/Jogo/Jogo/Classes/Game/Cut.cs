﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Jogo
{
    public class Cut
    {
        CutManager cut_manager;
        public Vector2 pos;
        public int life_spam;
        public int alive;
        static Texture2D pixel;
        SpriteBatch spriteBatch;
        public void Initialize(SpriteBatch spriteBatch)
        {
            if (pixel == null)
            {
                pixel = new Texture2D(cut_manager.game.GraphicsDevice, 1, 1);
                pixel.SetData(new[] { Color.SteelBlue });
                //pixel = cut_manager.game.Content.Load<Texture2D>("fire2");
            }
            // VC says I never set pixel, which is clearly wrong.
            if (spriteBatch == null)
                this.spriteBatch = spriteBatch;
        }
        public Cut(Vector2 p, CutManager ct)
        {
            cut_manager = ct;
            pos.X = p.X - ((int)p.X) % 10;
            pos.Y = p.Y - ((int)p.Y) % 10;
            life_spam = 700;
            alive = 1;
        }
        public void Update() 
        {
            if (alive == 0)
            {
                return;
            }
            cut_manager.set_field(pos, 1);
            life_spam--;
            if (life_spam < 0)
            {
                cut_manager.set_field(pos, 0);
                alive = 0;
                // Possível fonte de BUGS
                // Eu posso acabar setando um lugar que tinha um outro corte por cima para zero]
                // Para isso eu garanto que todo corte sempre atualiza sua marcação de espaço,
                // porém só retira sua marcação uma vez.
            }
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw a single red pixel
            if (pixel == null)
            {
                pixel = new Texture2D(cut_manager.game.GraphicsDevice, 1, 1);
                pixel.SetData(new[] { Color.White });
            }
            if (spriteBatch == null)
                this.spriteBatch = spriteBatch;
            spriteBatch.Draw(Cut.pixel, new Rectangle((int)pos.X + 2, (int)pos.Y + 2, 6, 6), Color.White);
        }
    }
    
}
