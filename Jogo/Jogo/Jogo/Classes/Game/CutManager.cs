﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Jogo
{
    public class CutManager
    {
        public Game1 game;
        public SpriteBatch spriteBatch;
        public List<Cut> cuts;
        public int[] field;
        public HoleManager hole_manager;
        private SoundEffect Death;

        public CutManager(Game1 g)
        {
            hole_manager = new HoleManager(this);
            Bomb.hole_manager = hole_manager;
            game = g;
            cuts = new List<Cut>();
            field = new int[Game1.ScreenWidth * Game1.ScreenHeight];
            // Preciso usar field como um array [1000][600]
        }
        public void Initialize(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
            foreach (Cut c in cuts)
            {
                c.Initialize(spriteBatch);
            }
            hole_manager.Initialize(spriteBatch);

            Death = game.Content.Load<SoundEffect>("Sound/water");
        }
        public int get_field(Vector2 v)
        {
            if (v.X < 0 || v.Y < 0 || v.X >= Game1.ScreenWidth || v.Y >= Game1.ScreenHeight)
                return -10;
            v.X -= ((int)v.X) % 10;
            v.Y -= ((int)v.Y) % 10;
            return field[(int)v.X * Game1.ScreenHeight + (int)v.Y];
        }
        public void set_field(Vector2 V, int v)
        {
            V.X -= ((int)V.X) % 10;
            V.Y -= ((int)V.Y) % 10;
            field[(int)(V.X) *Game1.ScreenHeight + (int)(V.Y)] = v;
        }
        public void Add(Cut c)
        {
            cuts.Add(c);
            c.Initialize(spriteBatch); 
            this.set_field(c.pos, 1);
        }
        public void Update()
        {
            for (int i = 0; i < cuts.Count; i++)
                if (cuts[i].alive == 0)
                {
                    cuts.RemoveAt(i);
                    i--;
                    // Possível fontes de BUGS, eu não sei o que acontece quando eu removo
                }
            foreach (Cut c in cuts)
            {
                c.Update();
            }
            hole_manager.Update();
            List<Vector2> pos = new List<Vector2>();
            pos.Add(game.player1.Position);
            pos.Add(game.player2.Position);

            foreach (Vector2 p in pos)
            {
                /*
                if (game.player1.Position == game.player1.last_position && p == pos[0])
                    continue;
                if (game.player2.Position == game.player2.last_position && p == pos[1])
                    continue;
                */
                if (get_field(p) == 0)
                {
                    set_field(p, 1);
                    this.Add(new Cut(p, this));
                }
                if (get_field(p) == 1)
                {
                    if (hole_manager.makeHole())
                    {
                        if (p == pos[0])
                        {
                            game.player1.Immortal();
                        }
                        else
                        {
                            game.player2.Immortal();
                        }
                    }
                }
                if (get_field(p) == 2 || get_field(p) == 1)
                {
                    if (p == pos[0])
                    {
                        double frac = 0, div = 0;
                        for (int i = -20; i <= 20; i++)
                        {
                            for (int j = -20; j <= 20; j++)
                            {
                                div++;
                                if (get_field(new Vector2(p.X + i, p.Y + j)) == 2 ||
                                    get_field(new Vector2(p.X + i, p.Y + j)) == 1)
                                {
                                    frac++;
                                }
                            }
                        }
                        frac /= div;
                        if (frac > 0.9)
                        {
                            game.player1.Kill();

                        }
                    }
                    if (p == pos[1])
                    {
                        double frac = 0, div = 0;
                        for (int i = -20; i <= 20; i++)
                        {
                            for (int j = -20; j <= 20; j++)
                            {
                                div++;
                                if (get_field(new Vector2(p.X + i, p.Y + j)) == 2 ||
                                    get_field(new Vector2(p.X + i, p.Y + j)) == 1)
                                {
                                    frac++;
                                }
                            }
                        }
                        frac /= div;
                        if (frac > 0.9)
                        {
                            game.player2.Kill();
                        }
                    }
                    continue;
                }
            }
        }
        public void Draw()
        {
            foreach (Cut c in cuts)
            {
                c.Draw(spriteBatch);
            }
            hole_manager.Draw();
        }
    }
}
