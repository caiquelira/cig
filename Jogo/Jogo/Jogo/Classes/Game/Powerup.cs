﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Jogo
{
    public class Powerup
    {
        //Atributos
        public bool activated=false;
        public bool on_screen = false;
        public Vector2 position;
        public static int timer = 0;

        private static SpriteBatch spriteBatch;
        
        public Texture2D texture;

        public int type;

        public Powerup()
        {

        }
 

        public static void Initialize(SpriteBatch spriteBatch)
        {
            //Powerup.texture = texture;
            Powerup.spriteBatch = spriteBatch;

        }

        public void Update(GameTime gameTime, Player player1, Player player2)
        {

            if (!activated) {
            timer++;

                if (timer > 500)
                {
                    activated = true;
                }
            }

            if (activated && !on_screen)
            {
                //randomize position of newley created powerup
                position = new Vector2();
                Random Ramdom = new Random();

                while (true)
                {
                    int randomNumberX = Ramdom.Next(60, Game1.ScreenWidth - 60);
                    this.position.X = randomNumberX;

                    int randomNumberY = Ramdom.Next(60, Game1.ScreenHeight - 60);
                    this.position.Y = randomNumberY;
                    activated = true;

                    //randomize type type powerup
                    type = Ramdom.Next(0, 4);
                    on_screen = true;
                    //Para impedir que o spawn do powerup caia dentro de um player
                    if ((player1.position.X - position.X) * (player1.position.Y - position.Y) < 1000)
                        continue;
                    if ((player2.position.X - position.X) * (player2.position.Y - position.Y) < 1000)
                        continue;
                    else
                        break;
                }
            }
        }

        public void Draw()
        {
            if (activated)
            {
                    spriteBatch.Draw(
                        texture,
                        position,
                        new Rectangle(type*texture.Width/4,0,
                           texture.Width / 4 ,(int)texture.Height),
                        Color.White,
                        0,
                        new Vector2((float)texture.Width / 4, (float)texture.Height),
                        1,
                        SpriteEffects.None,
                        0.0f);
            }
        }

        


    }
}
