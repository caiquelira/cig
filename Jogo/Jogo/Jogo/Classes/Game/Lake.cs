﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Jogo
{
    class Lake
    {
        //Atributos
        private static SpriteBatch spriteBatch;
        public static Texture2D texture;

        //Metodos
        public static void Initialize( SpriteBatch spriteBatch)
        {
            Lake.spriteBatch = spriteBatch;
            //Lake.texture = texture;
        }

        public void Draw()
        {
            spriteBatch.Draw(
                texture,     // A textura a ser desenhada
                new Vector2 (0, 0),    // A posição (vetorial) na qual desenhar
                null,               // O retângulo na textura de origem a ser desenhado (null -> toda a textura)
                Color.White,     // O filtro de cor a ser aplicado sobre a textura (branco -> deixa todas as cores iguais)
                0,       // O ângulo do qual rotacionar a imagem
                new Vector2(0,0), // O ponto dentro da textura que vai ser desenhado no ponto indicado no segundo parâmetro
                1,                  // A escala da imagem
                SpriteEffects.None, // Efeito (reflexão horizontal/vertical)
                0.0f);                 // Profundidade. Se menor, vai ser desenhado em cima. (Não usem isto. Desnhem na ordem certa no código mesmo.)
        }     
    }

}
