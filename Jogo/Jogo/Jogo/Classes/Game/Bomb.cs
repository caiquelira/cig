﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;

namespace Jogo
{
    public class Bomb
    {
        public Vector2 position;
        public static Texture2D texture;

        public static Texture2D explosion1;
        public static Texture2D explosion2;
        public static Texture2D explosion3;
        public static Texture2D explosion4;

        private static SpriteBatch spriteBatch;
        public static Game1 game;
        public static CutManager cut_manager;
        public static HoleManager hole_manager;

        float angle, speed;
        private float spinning_angle;

        public bool on_screen = false;
        public int bomb_timer = 0;
        public bool activated;
        public static SoundEffect explosion;
        public int exploding_counter;
        public bool exploding = false;

       
        protected int timer_explosao = 0;

        public Bomb(float speed, int bomb_timer)
        {
            this.speed = speed;
            this.bomb_timer = bomb_timer;
        }

        public static void Initialize(SpriteBatch spriteBatch)
        {
            Bomb.spriteBatch = spriteBatch;

        }

        //public void Load(Texture2D texture)
        //{
        //    this.texture = texture;

        //}
        public void Update(GameTime gameTime, Player player)
        {
            float dt = (float)gameTime.ElapsedGameTime.TotalSeconds;
            

            if (activated)
            {
                bomb_timer = 0;
                on_screen = true;
                activated = false;
                position = player.position;
                angle = player.Angle;
                spinning_angle = angle;
            }
            if (on_screen)
            {
                bomb_timer++;
               position += speed * new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * dt;
               spinning_angle += 0.2f;

               if (bomb_timer > 100)
                   Explode();
            }

        }

        public void Draw(Player player)
        {

            //trajetoria da bomba
            if (bomb_timer < 100 && on_screen)
            {
                spriteBatch.Draw(
                    texture,
                    position,
                    null,
                    Color.White,
                    spinning_angle,
                    new Vector2((float)texture.Width / 2, (float)texture.Height / 2),
                    1,
                    SpriteEffects.None,
                    0.0f);
            }
            //explosao
            else if (exploding_counter < 15 && exploding)
            {
                spriteBatch.Draw(
                  explosion1,
                  position,
                  null,
                  Color.White,
                  0,
                  new Vector2((float)texture.Width / 2, (float)texture.Height / 2),
                  1,
                  SpriteEffects.None,
                  0.0f);
            }
            else if (exploding_counter >= 15 && exploding_counter < 30 && exploding)
            {
                spriteBatch.Draw(
                  explosion2,
                  position,
                 null,
                  Color.White,
                  spinning_angle,
                  new Vector2((float)texture.Width / 2, (float)texture.Height / 2),
                  1,
                  SpriteEffects.None,
                  0.0f);
            }
            else if (exploding_counter >= 30 && exploding_counter < 45 && exploding)
            {
                spriteBatch.Draw(
                  explosion3,
                  position,
                 null,
                  Color.White,
                  spinning_angle,
                  new Vector2((float)texture.Width / 2, (float)texture.Height / 2),
                  1,
                  SpriteEffects.None,
                  0.0f);
            }
            else if (exploding_counter >= 45 && exploding_counter < 60 && exploding)
            {
                spriteBatch.Draw(
                  explosion4,
                  position,
                  null,
                  Color.White,
                  spinning_angle,
                  new Vector2((float)texture.Width / 2, (float)texture.Height / 2),
                  1,
                  SpriteEffects.None,
                  0.0f);
            }
            if (exploding_counter == 40)
            {
                exploding_counter = 0;
                exploding = false;
            }

            if (exploding)
            exploding_counter++;
            
        }

        public void Explode()
        {
            if (on_screen == false)
                return;
            on_screen = false;
            bomb_timer = 0;
            exploding = true;
            timer_explosao = 0;
            int explosion_dist = 100;
            explosion.Play();
            exploding = true;
            for (int i = -explosion_dist; i < explosion_dist; i += 10)
            {
                for (int j = -explosion_dist; j < explosion_dist; j += 10)
                {
                    Vector2 now = new Vector2(position.X + i, position.Y + j);
                    if (cut_manager.get_field(now) == -10)
                        continue;
                    if ((now.X - position.X) * (now.X - position.X) + (now.Y - position.Y) * (now.Y - position.Y)
                        < explosion_dist * explosion_dist)
                        hole_manager.Add(now);
                }
            }
        }

    }
}
