﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Jogo
{
    public class Player
    {
        //Fields
        public static Game1 game;
        public Vector2 position, last_position;
        private Vector2 speed_vector;
        public bool power_was_released;
        private float speed, angle, aceleration;
        public float MAX_ACELERATION = 200;
        public int alive;
        public int immortal;
        public bool imovel = false;

        //Referentes aos powerups
        public int player_type;
        public bool powerup_effect=false;
        public int powerup_timer = 0;
        public int InverterControles = 1;
        public bool HasBomb = false;

        private int id;
        private int raio = 23; //NAO ESQUECERR
        private static Viewport viewport;
        private static SpriteBatch spriteBatch;
        public Texture2D texture;

        SoundEffect Death = game.Content.Load<SoundEffect>("Sound/water");
        Texture2D invertido1 = game.Content.Load<Texture2D>("Textures/inverted1");
        Texture2D invertido2 = game.Content.Load<Texture2D>("Textures/inverted");


        //Properties
        //One of them refers to Position
        public Vector2 Position
        {
            get { return position; }
        }

        public float Angle { get { return angle; } }

        //Constructors
        public Player(Vector2 position, float speed, float angle, int id)
        {
            this.position = position;
            this.last_position = position;
            this.speed = speed;
            this.angle = angle;
            this.id = id;
            alive = 1;
        }

        //Methods

        public static void Initialize(Viewport viewport, SpriteBatch spriteBatch)
        {
            Player.viewport = viewport;
            Player.spriteBatch = spriteBatch;
           
        }
        
        public void Load(Texture2D texture)
        {
            this.texture = texture;


        }
        public void Immortal()
        {
            immortal = 0;
        }

        public void Update(GameTime gameTime, Player other_player, Bomb bomb, Slot slot, Powerup powerup)
        {
            last_position = position;
            if (immortal > 0)
                immortal--;
            if (id == 1 && !imovel)
            {
                    Vector2 inputDirection = Vector2.Zero;
                    if (Keyboard.GetState().IsKeyDown(Keys.Left))
                        angle -= 0.1f * InverterControles;
                    if (Keyboard.GetState().IsKeyDown(Keys.Right))
                        angle += 0.1f * InverterControles;
                    if (Keyboard.GetState().IsKeyDown(Keys.Up))
                        aceleration = MAX_ACELERATION;
                    if(Keyboard.GetState().IsKeyDown(Keys.Down))
                        speed_vector *= (float)0.93;
                    if(Keyboard.GetState().IsKeyDown(Keys.RightShift) && power_was_released)
                    {
                        Use_Powerup(powerup, slot, other_player);
                        if (HasBomb)
                        {
                            bomb.activated = true;
                            HasBomb = false;
                        }
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.RightShift))
                        power_was_released = false;
                    else
                        power_was_released = true;
            }

            else if (id ==2 && !imovel)
            {
                    Vector2 inputDirection = Vector2.Zero;
                    if (Keyboard.GetState().IsKeyDown(Keys.A))
                        angle -= 0.1f * InverterControles;
                    if (Keyboard.GetState().IsKeyDown(Keys.D))
                        angle += 0.1f * InverterControles;
                    if (Keyboard.GetState().IsKeyDown(Keys.W))
                        aceleration = MAX_ACELERATION;
                    if (Keyboard.GetState().IsKeyDown(Keys.S))
                        speed_vector *= (float)0.93;
                    if(Keyboard.GetState().IsKeyDown(Keys.Q) && power_was_released)
                    {
                        Use_Powerup(powerup, slot, other_player);
                        if (HasBomb)
                        {
                            bomb.activated = true;
                            HasBomb = false;
                        }
                    }
                    if (Keyboard.GetState().IsKeyDown(Keys.Q))
                        power_was_released = false;
                    else
                        power_was_released = true;
            }
            //Equacao horaria da posicao(vetorial) do MUV
            position += speed_vector*(float)gameTime.ElapsedGameTime.TotalSeconds + aceleration * new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * 
                (float)gameTime.ElapsedGameTime.TotalSeconds * (float)gameTime.ElapsedGameTime.TotalSeconds;

            //Equacao horaria da velocidade(vetorial) do MUV
            speed_vector += aceleration * new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) *(float)gameTime.ElapsedGameTime.TotalSeconds;
            
            //Mudar: criar classe para herdar as outras **************
            if (id ==1) {
            //Desaceleracao do Player quando nao houver input do usuario
                if (speed_vector != new Vector2(0, 0) && !Keyboard.GetState().IsKeyDown(Keys.Up) && !Keyboard.GetState().IsKeyDown(Keys.Down))
                {
                    speed_vector *= (float)0.975;
                }
                else
                    aceleration = 0;
            }

            else {

                 if (speed_vector != new Vector2(0, 0) && !Keyboard.GetState().IsKeyDown(Keys.W) && !Keyboard.GetState().IsKeyDown(Keys.S))
                {
                    speed_vector *= (float)0.975;
                }

                else
                aceleration = 0;
            }
            //Condicao para haver colisao com a parede
            if (Check_Collision_Wall())
                Collision_Wall();

            //Terminar o powerup
            Cancel_PowerUp(other_player);
            if (powerup_effect)
            {
                powerup_timer++;
                if (powerup_timer > 400)
                {
                    powerup_effect = false;
                    powerup_timer = 0;
                    other_player.imovel = false;
                }
            }

         //   if ((bomb.position.X - other_player.position.X) * (bomb.position.Y - other_player.position.Y) < raio * raio)
           //     bomb.Explode();
        }

        public void Draw(Powerup powerup)
        {

            Random Random = new Random();
            int random;
            Vector2 inputDirection = Vector2.Zero;

            if (Keyboard.GetState().IsKeyDown(Keys.Up) && id == 1)
                random = Random.Next(1, 3);
            else if (Keyboard.GetState().IsKeyDown(Keys.W) && id == 2)
                random = Random.Next(1, 3);
            else
                random = 0;

            if (random% 2 +1 ==0)
            //if (random % 2 == 0 && speed_vector.Length() > 10)
            {
                spriteBatch.Draw(
                    texture,
                    position,
                    new Rectangle(0, random * texture.Height / 5,
                    texture.Width, (int)texture.Height / 5),
                    Color.White,
                    angle,
                    new Vector2((float)texture.Width*65 / 90, (float)texture.Height / 10),
                    1,
                    SpriteEffects.FlipVertically,
                    0.0f);
            }
            else if (imovel)
            {
                if (random == 2)
                    random = 3;
                else random = 0;
                spriteBatch.Draw(
                   texture,
                   position,
                   new Rectangle(0, random * texture.Height / 5,
                   texture.Width, (int)texture.Height / 5),
                   Color.White,
                   angle,
                   new Vector2((float)texture.Width * 65 / 90, (float)texture.Height / 10),
                   1,
                   SpriteEffects.None,
                   0.0f);
            }
            else if (InverterControles == -1 && id == 1)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.Up) && id == 1)
                    random = Random.Next(0, 2);
                else
                    random = 0;
                spriteBatch.Draw(
                   invertido1,
                   position,
                   new Rectangle(0, random * invertido1.Height / 3,
                   invertido1.Width, (int)invertido1.Height / 3),
                   Color.White,
                   angle,
                   new Vector2((float)invertido1.Width * 65 / 90, (float)invertido1.Height / 6),
                   1,
                   SpriteEffects.None,
                   0.0f);
            }
            else if (InverterControles == -1 && id == 2)
            {
                if (Keyboard.GetState().IsKeyDown(Keys.W) && id == 2)
                    random = Random.Next(1, 3);
                else
                    random = 0;
                spriteBatch.Draw(
                   invertido2,
                   position,
                   new Rectangle(0, random * invertido2.Height / 3,
                   invertido2.Width, (int)invertido2.Height / 3),
                   Color.White,
                   angle,
                   new Vector2((float)invertido2.Width * 65 / 90, (float)invertido2.Height / 6),
                   1,
                   SpriteEffects.None,
                   0.0f);
            }

            else
            {
                spriteBatch.Draw(
                   texture,
                   position,
                   new Rectangle(0, random * texture.Height / 5,
                   texture.Width, (int)texture.Height / 5),
                   Color.White,
                   angle,
                   new Vector2((float)texture.Width * 65 / 90, (float)texture.Height / 10),
                   1,
                   SpriteEffects.None,
                   0.0f);
            }

        }

        public bool Check_Collision_Player(Player player)
        {

            if (raio * raio * 4 < (player.position.X - this.position.X) * (player.position.X - this.position.X) +
                (player.position.Y - this.position.Y) * (player.position.Y - this.position.Y))
                return false;
            else
                return true;

        }


        public bool Check_Collision_Wall()
        {
            if (position.X < texture.Width / 2) return true;
            if (position.Y < texture.Width / 2) return true;
            if (position.X > Game1.ScreenWidth - texture.Width / 2) return true;
            if (position.Y > Game1.ScreenHeight - texture.Width) return true;

            return false;
        }

        //Concertar as colisoes bidimensionais depois*******************
        public void Collision_Player(Player player)
        {
            if (Check_Collision_Player(player))
            {
                //Vector na direcao da colisao
                Vector2 direction = new Vector2(player.position.X - position.X, player.position.Y - position.Y);

                float angle = -(float)Math.Atan(player.position.Y - position.Y / player.position.X - position.X);

                speed_vector = RotateVector2(speed_vector, angle);
                player.speed_vector = RotateVector2(player.speed_vector, angle);


                float temp = speed_vector.X;
                speed_vector.X = player.speed_vector.X;
                player.speed_vector.X = temp;


                speed_vector = RotateVector2(speed_vector, -angle);
                player.speed_vector = RotateVector2(player.speed_vector, -angle);

                speed_vector += -2*direction;
                player.speed_vector += 2*direction;
            }
        }

        public static Vector2 RotateVector2(Vector2 point, float radians)
        {
            float cosRadians = (float)Math.Cos(radians);
            float sinRadians = (float)Math.Sin(radians);

            return new Vector2(
                point.X * cosRadians - point.Y * sinRadians,
                point.X * sinRadians + point.Y * cosRadians);
        }

        public void Collision_Wall()
        {
            //coeficiente de restituicao
            float e = 0.5f;
            if (position.X < texture.Width / 2)
            {
                speed_vector.X = Math.Abs(speed_vector.X);
                position.X = texture.Width / 2;
            }
            if (position.Y < texture.Width / 2)
            {
                speed_vector.Y = Math.Abs(speed_vector.Y);
                position.Y = texture.Width / 2;
            }
            if (position.X > Game1.ScreenWidth - texture.Width / 2)
            {
                speed_vector.X = -Math.Abs(speed_vector.X);
                position.X = Game1.ScreenWidth - texture.Width / 2;
            }
            if (position.Y > Game1.ScreenHeight - texture.Width)
            {
                speed_vector.Y = -Math.Abs(speed_vector.Y);
                position.Y = Game1.ScreenHeight - texture.Width;
            }

            speed_vector *= (float)e;
        }

        public bool Check_Collision_Powerup(Powerup powerup)
        {
            if (powerup.activated == false)
                return false;
            if (texture.Width * texture.Width / 2.5 < (powerup.position.X - this.position.X) * (powerup.position.X - this.position.X) +
               (powerup.position.Y - this.position.Y) * (powerup.position.Y - this.position.Y))
                return false;
            else
                return true;
        }

        public void Get_PowerUp(Powerup powerup, Player other_player, Slot slot)
        {
            if (Check_Collision_Powerup(powerup))
            {
                powerup.activated = false;
                powerup.on_screen = false;
                slot.Adicionar_Powerup(powerup);

            }

        }
        public void Cancel_PowerUp(Player other_player)
        {
            if (!powerup_effect)
            {
                MAX_ACELERATION = 200;
                other_player.InverterControles = 1;
            }

        }

        public void Kill()
        {
            {
                if (alive == 1)
                {
                    MediaPlayer.Pause();
                    Death.Play();
                }
                if (immortal == 0)
                    alive = 0;
            }

        }

        public void Use_Powerup(Powerup powerup, Slot slot, Player other_player)
        {
                if (slot.Slot1_free)
                    return;
                slot.Slot1_free = true;
                powerup_effect = true;
                if (slot.Slot1 == 0) //Dimiuir a velocidade do oponente
                {
                    other_player.InverterControles = -1;
                    Powerup.timer = 100;
                }

                else if (slot.Slot1 == 1) //Inverter os controles do oponente
                {
                    other_player.imovel = true;
                    Powerup.timer = 0;
                }

                else if (slot.Slot1 == 2)
                {
                    HasBomb = true;
                    Powerup.timer = 200;
                }
                

                else if (slot.Slot1==3)
                {
                    MAX_ACELERATION = 600;
                    Powerup.timer = 100;
                }
        }

    }

}
