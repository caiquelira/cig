﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Jogo
{
    public class Hole
    {
        public static CutManager cut_manager;
        public HoleManager hole_manager;
        public Vector2 pos;
        public int life_spam;
        public int alive;
        SpriteBatch spriteBatch;
        static Texture2D pixel;
        public void Initialize(SpriteBatch spriteBatch)
        {
            if (pixel == null)
            {
                pixel = new Texture2D(cut_manager.game.GraphicsDevice, 1, 1);
                pixel.SetData(new[] { Color.SteelBlue });
                //pixel = hole_manager.cut_manager.game.Content.Load<Texture2D>("fire");
            }
            if (spriteBatch == null) 
                this.spriteBatch = spriteBatch;
        }
        public Hole(Vector2 p, HoleManager hm)
        {
            hole_manager = hm;
            pos.X = p.X;
            pos.Y = p.Y;
            life_spam = 700;
            alive = 1;
        }
        public void Update()
        {
            if (alive == 0)
            {
                return;
            }
            cut_manager.set_field(pos, 2);
            life_spam--;
            if (life_spam < 0)
            {
                alive = 0;
                cut_manager.set_field(pos, 0);
            }
            // Preciso testar se existe algum player no buraco
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            // Draw a single black pixel
            if (pixel == null)
            {
                pixel = new Texture2D(cut_manager.game.GraphicsDevice, 1, 1);
                pixel.SetData(new[] { Color.White});
            }
            if (spriteBatch == null)
                this.spriteBatch = spriteBatch;
            spriteBatch.Draw(pixel, new Rectangle((int)pos.X, (int)pos.Y, 10, 10), Color.White*0.5f);
        }
    }
}
