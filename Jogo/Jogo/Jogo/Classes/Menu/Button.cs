﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Jogo
{
    public class Button
    {
        public static Game1 game;
        public Action action;
        bool cont_click, non_click;
        public Vector2 pos, end_pos, pos_unact;
        public static SpriteBatch spriteBatch;
        Texture2D activated, unactive;
        SoundEffect button_noise = game.Content.Load<SoundEffect>("button");

        public Button(Action new_act, Vector2 a, Vector2 b, Texture2D act, Texture2D unact)
        {
            this.activated = act;
            this.unactive = unact;
            cont_click = false;
            pos = a;
            pos_unact = b;
            end_pos = new Vector2();
            end_pos.X = pos.X + act.Width;
            end_pos.Y = pos.Y + act.Height;
            action = new_act;
        }
        public void Draw()
        {
            if (cont_click)
                spriteBatch.Draw(activated, pos, Color.White);
            else
                spriteBatch.Draw(unactive, pos_unact, Color.White);
        }
        public bool Inside()
        {
            MouseState ms = Mouse.GetState();
            int x = ms.X;
            int y = ms.Y;
            return x > pos.X && x < end_pos.X && y > pos.Y && y < end_pos.Y;
        }
        public void Update()
        {
            MouseState ms = Mouse.GetState();
            if (ms.LeftButton == ButtonState.Pressed)
            {
                if (!Inside())
                    cont_click = false;
                else if (non_click)
                {
                    cont_click = true;
                }
                non_click = false;
            }
            else
            {
                non_click = true;
                if (cont_click)
                {
                    button_noise.Play();
                    this.action.Invoke();
                }
                cont_click = false;
            }
        }
    }
}
