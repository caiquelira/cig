﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;
namespace Jogo
{
    class MenuManager
    {
        public static Game1 game;
        int menu_state;
        public static SpriteBatch spriteBatch;
        List<Menu> menus;
        public static Texture2D main_menu, play_game1, play_game2, instructions1, instructions2;
        public static Texture2D controls1, controls2, power1, power2;
        public static Texture2D intructions_menu, controls_menu, power_menu;
        public MenuManager()
        {
            MenuManager.spriteBatch = game.spriteBatch;
            Menu.spriteBatch = spriteBatch;
            MenuManager.main_menu = game.Content.Load<Texture2D>("main_menu");
            MenuManager.instructions1 = game.Content.Load<Texture2D>("instructions1");
            MenuManager.instructions2 = game.Content.Load<Texture2D>("instructions2");
            MenuManager.power1 = game.Content.Load<Texture2D>("power1");
            MenuManager.power2 = game.Content.Load<Texture2D>("power2");
            MenuManager.play_game1 = game.Content.Load<Texture2D>("play_game_1");
            MenuManager.play_game2 = game.Content.Load<Texture2D>("play_game2");
            MenuManager.controls1 = game.Content.Load<Texture2D>("controls1");
            MenuManager.controls2 = game.Content.Load<Texture2D>("controls2");
            MenuManager.intructions_menu = game.Content.Load<Texture2D>("instructions_menu");
            MenuManager.power_menu = game.Content.Load<Texture2D>("power_menu");
            MenuManager.controls_menu = game.Content.Load<Texture2D>("controls_menu");
            int deltax = (game.viewport.Width - 1020) / 2;
            int deltay = (game.viewport.Height - 760) / 2;
            menu_state = 0;
            // 0 => main menu
            // 1 => instructions menu
            // 2 => controsl menu
            // 3 => power ups menu
            menus = new List<Menu>();
            //Adicionar main menu
            Menu menu = new Menu(main_menu);
            //Fazer o botão de play game
            menu.Add(new Button(() =>
            {
                game.game_state = 2;
                Song song = game.Content.Load<Song>("Sound/GameSound");
                MediaPlayer.IsRepeating = true;
                MediaPlayer.Play(song);
            },
                new Vector2(570 + deltax, 478 + deltay),
                new Vector2(570 + deltax, 478 + deltay),
                play_game1,
                play_game2));
            //Fazer o botão que vai pra instructions
            menu.Add(new Button(() => { menu_state = 1; },
                new Vector2(102 + deltax, 478 + deltay),
                new Vector2(102 + deltax, 478 + deltay),
                instructions1,
                instructions2));
            menus.Add(menu);
            //Adicionar instructions menu
            menu = new Menu(intructions_menu);
            menu.Add(new Button(() => { menu_state = 2; },
                new Vector2(-5 + deltax, 617 + deltay),
                new Vector2(-10 + deltax, 613 + deltay),
                controls1,
                controls2));
            menu.Add(new Button(() => { menu_state = 3; },
                new Vector2(339 + deltax, 606 + deltay),
                new Vector2(350 + deltax, 615 + deltay),
                power1,
                power2));
            menu.Add(new Button(() =>
            {
                game.game_state = 2;
                Song song = game.Content.Load<Song>("Sound/GameSound");
                MediaPlayer.IsRepeating = true;
                MediaPlayer.Play(song);
            },
                new Vector2(683 + deltax, 610 + deltay),
                new Vector2(685 + deltax, 613 + deltay),
                play_game1,
                play_game2));
            menus.Add(menu);
            //Adicionar controls menu
            menu = new Menu(controls_menu);
            menu.Add(new Button(() => { menu_state = 1; },
                new Vector2(-10 + deltax, 617 + deltay),
                new Vector2(-15 + deltax, 617 + deltay),
                instructions1,
                instructions2));
            menu.Add(new Button(() => { menu_state = 3; },
                new Vector2(339 + deltax, 606 + deltay),
                new Vector2(350 + deltax, 615 + deltay),
                power1,
                power2));
            menu.Add(new Button(() =>
            {
                game.game_state = 2;
                Song song = game.Content.Load<Song>("Sound/GameSound");
                MediaPlayer.IsRepeating = true;
                MediaPlayer.Play(song);
            },
                new Vector2(683 + deltax, 610 + deltay),
                new Vector2(685 + deltax, 613 + deltay),
                play_game1,
                play_game2));
            menus.Add(menu);
            //Adicionar power menu
            menu = new Menu(power_menu);
            menu.Add(new Button(() => { menu_state = 1; },
                new Vector2(-10 + deltax, 617 + deltay),
                new Vector2(-15 + deltax, 617 + deltay),
                instructions1,
                instructions2));
            menu.Add(new Button(() => { menu_state = 2; },
                new Vector2(350 + deltax, 615 + deltay),
                new Vector2(338 + deltax, 610 + deltay),
                controls1,
                controls2));
            menu.Add(new Button(() =>
            {
                game.game_state = 2;
                Song song = game.Content.Load<Song>("Sound/GameSound");
                MediaPlayer.IsRepeating = true;
                MediaPlayer.Play(song);
            },
                new Vector2(683 + deltax, 610 + deltay),
                new Vector2(685 + deltax, 613 + deltay),
                play_game1,
                play_game2));
            menus.Add(menu);
        }
        public void Draw()
        {
            if (game.game_state != 1)
                return;
            menus[menu_state].Draw();
        }
        public void Update()
        {
            if (game.game_state != 1)
                return;
            menus[menu_state].Update();
        }
    }
}
