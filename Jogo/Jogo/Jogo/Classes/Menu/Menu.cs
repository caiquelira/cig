﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;
namespace Jogo
{
    class Menu
    {
        public Texture2D texture;
        public static Game1 game;
        List<Button> buttons;
        public static SpriteBatch spriteBatch;
        public Menu(Texture2D text)
        {
            buttons = new List<Button>();
            this.texture = text;
            int n = texture.Width;
            Song song = game.Content.Load<Song>("Sound/menu");
            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(song);
        }
        public void Add(Button b)
        {
            buttons.Add(b);
        }
        public void Update()
        {
            foreach (Button b in buttons)
                b.Update();
        }
        public void Draw()
        {
            spriteBatch.Draw(texture, new Vector2((game.viewport.Width - 1020) / 2, (game.viewport.Height - 760) / 2), Color.White);
            foreach (Button b in buttons)
                b.Draw();
        }
    }
}
