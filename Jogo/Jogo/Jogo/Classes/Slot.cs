﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Jogo
{
    public class Slot
    {
        Vector2 position;
        public static Texture2D texture;
        private static SpriteBatch spriteBatch;

        int slot1, slot2, NumPowerUps;
        Vector2 position_slot1;
        Vector2 position_slot2;
        public bool Slot1_free, Slot2_free;

        public float Slot1 { get { return slot1; } }
        public float Slot2 { get { return slot2; } }

        public Slot(Vector2 position_slot1, Vector2 position_slot2)
        {
            this.position_slot1 = position_slot1;
            this.position_slot2 = position_slot2;

        }



        public void Initialize(SpriteBatch spriteBatch)
        {
            Slot.spriteBatch = spriteBatch;
            Slot1_free = true;
            Slot2_free = true;
        }

        public void Update(GameTime gameTime, Player player)
        {
            Clean_Up_Slot();
        }

        public void Draw()
        {
                //Draw Slot 1
            if (!Slot1_free)
            {
                spriteBatch.Draw(
                    texture,
                    position_slot1,
                    new Rectangle(slot1*texture.Width/4,0,
                           texture.Width / 4 ,(int)texture.Height),
                    Color.White,
                    0,
                    new Vector2((float)texture.Width / 2, (float)texture.Height / 2),
                    1,
                    SpriteEffects.None,
                    0.0f);
            }

            //Draw Slot 2
            if (!Slot2_free)
            {
                spriteBatch.Draw(
                        texture,
                        position_slot2,
                        new Rectangle(slot2*texture.Width/4,0,
                           texture.Width / 4 ,(int)texture.Height),
                        Color.White,
                        0,
                        new Vector2((float)texture.Width / 4, (float)texture.Height),
                        1,
                        SpriteEffects.None,
                        0.0f);
            }
            
        }


        public void Adicionar_Powerup(Powerup powerup)
        {
            if (Slot1_free)
            {
                slot1 = powerup.type;
                Slot1_free = false;
            }

            else if (!Slot1_free && Slot2_free)
            {
                slot2 = powerup.type;
                Slot2_free = false;
            }

            else 
            {
                int temp = slot2;
                slot2 = powerup.type;
                slot1 = temp;
            }
        }

        public void Clean_Up_Slot()
        {
            if (Slot1_free && !Slot2_free)
            {
                slot1 = slot2;
                Slot2_free = true;
                Slot1_free = false;
            }
        }

    }
}

