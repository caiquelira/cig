﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Jogo
{

    class HoleManager
    {
        int[] filax, filay;
        SpriteBatch spriteBatch;
        List<Hole> holes;
        public CutManager cut_manager;
        public HoleManager(CutManager c)
        {
            filax = new int[600000];
            filay = new int[600000];
            cut_manager = c;
            holes = new List<Hole>();
        }
        public void Initialize(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
            foreach (Hole h in holes)
            {
                h.Initialize(spriteBatch);
            }
        }
        public void Update()
        {
            for (int i = 0; i < holes.Count; i++)
                if (holes[i].alive == 0)
                {
                    holes.RemoveAt(i);
                    i--;
                    // Fonte de BUGS, não sei o que acontece quando eu deleto um elemento
                }
            foreach (Hole h in holes)
            {
                h.Update();
            }
        }
        public void Add(Vector2 pos)
        {
            Hole h = new Hole(pos, this);
            h.Initialize(spriteBatch);
            holes.Add(h);
            cut_manager.set_field(pos, 2);
            // Problema: ele adiciona o mesmo buraco várias vezes
        }
        public bool makeHole()
        {
            // Preciso primeiro garantir que tenho acesso aos fields
            // Preciso zerar as visitas de cada pixel
            int nholes = holes.Count();
            for (int i = 0; i < 1000; i += 10)
                for (int j = 0; j < 600; j += 10)
                {
                    if (cut_manager.field[i * 600 + j] == 0)
                        cut_manager.field[i * 600 + j] = -1;
                }
            int fim = 0, ini = 0;
            int[] dx = { 0, 0, -1, 1 }, dy = { -1, 1, 0, 0 };
            // Adiciona todos os pontos das bordas na BFS
            fim = 0;
            for (int i = 0; i < 1000; i += 10)
            {
                filax[fim] = i;
                filay[fim++] = 0;
                cut_manager.field[i * 600 + 0] = 0;
                filax[fim] = i;
                filay[fim++] = 590; //599
                cut_manager.field[i * 600 + 590] = 0;
            }
            for (int j = 10; j <= 580; j += 10) //1 e 598
            {
                filax[fim] = 0;
                filay[fim++] = j;
                cut_manager.field[0 * 600 + j] = 0;
                filax[fim] = 990; //999
                filay[fim++] = j;
                cut_manager.field[990 * 600 + j] = 0;
            }
            // Para cada cara, tenta adicionar seus vizinhos, tomando cuidado com as exceções
            while (ini < fim)
            {
                int nowx = filax[ini];
                int nowy = filay[ini];
                ini++;
                //Debug.WriteLine(ini.ToString() + " " + fim.ToString() + " " + nowx.ToString() + " " + nowy.ToString());
                for (int d = 0; d < 4; d++)
                {
                    int vizx = nowx + dx[d] * 10;
                    int vizy = nowy + dy[d] * 10;
                    if (vizx < 0 || vizy < 0 || vizx >= 1000 || vizy >= 600)
                    {
                        continue;
                    }
                    if (cut_manager.field[vizx * 600 + vizy] == -1)
                    {
                        cut_manager.field[vizx * 600 + vizy] = 0;
                        filax[fim] = vizx;
                        filay[fim++] = vizy;
                    }
                }
            }
            // Verifica quais caras não foram visitados e que não tinham sido marcados ainda como buracos
            // Dando o mesmo life_spam para eles, cada pixel deles será um hole
            for (int i = 0; i < 1000; i += 10)
            {
                for (int j = 0; j < 600; j += 10)
                {
                    if (cut_manager.field[i * 600 + j] != -1)
                        continue;
                    Vector2 pos;
                    pos.X = i;
                    pos.Y = j;
                    this.Add(pos);
                }
            }
            return holes.Count() > nholes;
        }
        public void Draw()
        {
            foreach (Hole h in holes)
            {
                h.Draw(spriteBatch);
            }
        }
    }
}
