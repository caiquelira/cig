﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Diagnostics;

namespace Jogo
{
    class Cut
    {
        CutManager cut_manager;
        public Vector2 pos;
        public int life_spam;
        public int alive;
        Texture2D pixel;
        SpriteBatch spriteBatch;
        public void Initialize(SpriteBatch spriteBatch)
        {
            pixel = new Texture2D(cut_manager.game.GraphicsDevice, 1, 1);
            pixel.SetData(new[] { Color.White });
            // VC says I never set pixel, which is clearly wrong.
            this.spriteBatch = spriteBatch;
        }
        public Cut(Vector2 p, CutManager ct)
        {
            cut_manager = ct;
            pos.X = p.X - ((int)p.X) % 10;
            pos.Y = p.Y - ((int)p.Y) % 10;
            life_spam = 1000;
            alive = 1;
        }
        public void Update() 
        {
            if (alive == 0)
            {
                return;
            }
            cut_manager.set_field(pos, 1);
            life_spam--;
            if (life_spam < 0)
            {
                cut_manager.set_field(pos, 0);
                alive = 0;
                // Possível fonte de BUGS
                // Eu posso acabar setando um lugar que tinha um outro corte por cima para zero]
                // Para isso eu garanto que todo corte sempre atualiza sua marcação de espaço,
                // porém só retira sua marcação uma vez.
            }
        }
        public void Draw()
        {
            // Draw a single red pixel
            spriteBatch.Draw(pixel, new Rectangle((int)pos.X, (int)pos.Y, 5, 5), Color.White);
        }
    }
    class Hole
    {
        public HoleManager hole_manager;
        public Vector2 pos;
        public int life_spam;
        public int alive;
        SpriteBatch spriteBatch;
        Texture2D pixel;
        public void Initialize(SpriteBatch spriteBatch)
        {
            pixel = new Texture2D(hole_manager.cut_manager.game.GraphicsDevice, 1, 1);
            pixel.SetData(new[] { Color.Blue });
            this.spriteBatch = spriteBatch;
        }
        public Hole(Vector2 p, HoleManager hm)
        {
            hole_manager = hm;
            pos.X = p.X;
            pos.Y = p.Y;
            life_spam = 1000;
            alive = 1;
        }
        public void Update()
        {
            if (alive == 0)
            {
                return;
            }
            hole_manager.cut_manager.set_field(pos, 2);
            life_spam--;
            if (life_spam < 0)
            {
                alive = 0;
                hole_manager.cut_manager.set_field(pos, 0);
            }
            // Preciso testar se existe algum player no buraco
        }
        public void Draw()
        {
            // Draw a single black pixel
            spriteBatch.Draw(pixel, new Rectangle((int)pos.X, (int)pos.Y, 2, 2), Color.Purple);
        }
    }
    class HoleManager
    {
        int[] filax, filay;
        SpriteBatch spriteBatch;
        List<Hole> holes;
        public CutManager cut_manager;
        public HoleManager(CutManager c)
        {
            filax = new int[600000];
            filay = new int[600000];
            cut_manager = c;
            holes = new List<Hole>();
        }
        public void Initialize(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
            foreach (Hole h in holes)
            {
                h.Initialize(spriteBatch);
            }
        }
        public void Update()
        {
            for (int i = 0; i < holes.Count; i++)
                if (holes[i].alive == 0)
                {
                    holes.RemoveAt(i);
                    i--;
                    // Fonte de BUGS, não sei o que acontece quando eu deleto um elemento
                }
            foreach (Hole h in holes)
            {
                h.Update();
            }
        }
        public void Add(Vector2 pos)
        {
            Hole h = new Hole(pos, this);
            h.Initialize(spriteBatch);
            holes.Add(h);
            cut_manager.set_field(pos, 2);
            // Problema: ele adiciona o mesmo buraco várias vezes
        }
        public void makeHole()
        {
            // Preciso primeiro garantir que tenho acesso aos fields
            // Preciso zerar as visitas de cada pixel
            for (int i = 0; i < 1000; i += 10)
                for (int j = 0; j < 600; j += 10)
                {
                    if (cut_manager.field[i*600 + j] == 0)
                        cut_manager.field[i*600 + j] = -1;
                }
            int fim = 0, ini = 0;
            int[] dx = { 0, 0, -1, 1 }, dy = { -1, 1, 0, 0 };
            // Adiciona todos os pontos das bordas na BFS
            fim = 0;
            for (int i = 0; i < 1000; i += 10)
            {
                filax[fim] = i;
                filay[fim++] = 0;
                cut_manager.field[i*600 + 0] = 0;
                filax[fim] = i;
                filay[fim++] = 590; //599
                cut_manager.field[i*600 + 590] = 0;
            }
            for (int j = 10; j <= 580; j += 10) //1 e 598
            {
                filax[fim] = 0;
                filay[fim++] = j;
                cut_manager.field[0*600 + j] = 0;
                filax[fim] = 990; //999
                filay[fim++] = j;
                cut_manager.field[990*600 + j] = 0;
            }
            // Para cada cara, tenta adicionar seus vizinhos, tomando cuidado com as exceções
            while (ini < fim)
            {
                int nowx = filax[ini];
                int nowy = filay[ini];
                ini++;
                //Debug.WriteLine(ini.ToString() + " " + fim.ToString() + " " + nowx.ToString() + " " + nowy.ToString());
                for (int d = 0; d < 4; d++)
                {
                    int vizx = nowx + dx[d]*10;
                    int vizy = nowy + dy[d]*10;
                    if (vizx < 0 || vizy < 0 || vizx >= 1000 || vizy >= 600)
                    {
                        continue;
                    }
                    if (cut_manager.field[vizx*600 + vizy] == -1)
                    {
                        cut_manager.field[vizx*600 + vizy] = 0;
                        filax[fim] = vizx;
                        filay[fim++] = vizy;
                    }
                }
            }
            // Verifica quais caras não foram visitados e que não tinham sido marcados ainda como buracos
            // Dando o mesmo life_spam para eles, cada pixel deles será um hole
            for (int i = 0; i < 1000; i += 10)
            {
                for (int j = 0; j < 600; j += 10)
                {
                    if (cut_manager.field[i*600 + j] != -1)
                        continue;
                    Vector2 pos;
                    pos.X = i;
                    pos.Y = j;
                    this.Add(pos);
                }
            }
        }
        public void Draw() 
        {
            foreach (Hole h in holes)
            {
                h.Draw();
            }
        }
    }
    class CutManager
    {
        public Game1 game;
        SpriteBatch spriteBatch;
        List<Cut> cuts;
        public int[] field;
        HoleManager hole_manager;
        public CutManager(Game1 g)
        {
            hole_manager = new HoleManager(this);
            game = g;
            cuts = new List<Cut>();
            field = new int[600000];
            // Preciso usar field como um array [1000][600]
        }
        public void Initialize(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
            foreach (Cut c in cuts)
            {
                c.Initialize(spriteBatch);
            }
            hole_manager.Initialize(spriteBatch);
        }
        public int get_field(Vector2 v) 
        {
            v.X -= ((int)v.X) % 10;
            v.Y -= ((int)v.Y) % 10;
            return field[(int)v.X*600 + (int)v.Y];
        }
        public void set_field(Vector2 V, int v)
        {
            V.X -= ((int)V.X) % 10;
            V.Y -= ((int)V.Y) % 10;
            field[(int)(V.X)*600 + (int)(V.Y)] = v;
        }
        public void Add(Cut c) 
        {
            cuts.Add(c);
            c.Initialize(spriteBatch);
            this.set_field(c.pos, 1);
        }
        public void Update()
        {
            for (int i = 0; i < cuts.Count; i++)
                if (cuts[i].alive == 0)
                {
                    cuts.RemoveAt(i);
                    i--;
                    // Possível fontes de BUGS, eu não sei o que acontece quando eu removo
                }
            foreach (Cut c in cuts)
            {
                c.Update();
            }
            hole_manager.Update();
            List<Vector2> pos = new List<Vector2>();
            pos.Add(game.player1.Position);
            pos.Add(game.player2.Position);
            
            foreach (Vector2 p in pos)
            {
                /*
                if (game.player1.Position == game.player1.last_position && p == pos[0])
                    continue;
                if (game.player2.Position == game.player2.last_position && p == pos[1])
                    continue;
                */
                if (get_field(p) == 2)
                {
                    if (p == pos[0])
                        game.player1.Kill();
                    if (p == pos[1])
                        game.player2.Kill();
                    continue;
                }
                if (get_field(p) == 1)
                {
                    hole_manager.makeHole();
                }
                else
                {
                    set_field(p, 1);
                    this.Add(new Cut(p, this));
                }
            }
        }
        public void Draw()
        {
            foreach (Cut c in cuts)
            {
                c.Draw();
            }
            hole_manager.Draw();
        }
    }
}
