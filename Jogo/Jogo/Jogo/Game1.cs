using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;

namespace Jogo
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;
        public Viewport viewport;

        //Dimensoes do mapa
        public const int ScreenWidth = 1400;
        public const int ScreenHeight = 800;

        public SpriteFont font; //Depois deletar

        public bool play_again;
        public int game_state;
      
        public Player player1;
        public Player player2;
        public Bomb bomb1;
        public Bomb bomb2;
        public Button play, exit;
        public Slot slot1;
        public Slot slot2;
        Lake frozen_lake;
        Powerup powerup;
        public Texture2D mousePointer;
        public Texture2D slot;
        private Vector2 cursorPos;

        public CutManager cut_manager;

        MenuManager menu;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            //graphics.IsFullScreen = true;
            Content.RootDirectory = "Content";
            play_again = true;

            //Definir o tamanho da tela
            graphics.PreferredBackBufferWidth = ScreenWidth;
            graphics.PreferredBackBufferHeight = ScreenHeight;
            //graphics.PreferMultiSampling = false;
            //graphics.IsFullScreen = true;
        }

        protected override void Initialize()
        {
            viewport = graphics.GraphicsDevice.Viewport;
            game_state = 1; // 1 = Game Menu
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            cut_manager = new CutManager(this);
            //Inicializacao dos objetos
            Player.Initialize(viewport, spriteBatch);
            Player.game = this;
            Bomb.Initialize(spriteBatch);
            Lake.Initialize(spriteBatch);
            Powerup.Initialize(spriteBatch);
            cut_manager.Initialize(spriteBatch);
            
            //Instancia dos objetos
            player1 = new Player(new Vector2(400), 15, 0, 1);
            player2 = new Player(new Vector2(400, 500), 15, 0, 2);
            bomb1 = new Bomb(200.0f, 0);
            bomb2 = new Bomb(200.0f, 0);
            slot2 = new Slot(new Vector2(289,763), new Vector2(312,793));
            slot1 = new Slot(new Vector2(1225,762), new Vector2(1250,792));
            slot1.Initialize(spriteBatch);
            slot2.Initialize(spriteBatch);
            frozen_lake = new Lake();
            powerup = new Powerup();


            //Loading Textures, Sound and Font
            player1.texture = Content.Load<Texture2D>("Textures/bluespaceship");
            player2.texture = Content.Load<Texture2D>("Textures/orangespaceship");
            powerup.texture = Content.Load<Texture2D>("Textures/powerups");
            Bomb.texture = Content.Load<Texture2D>("Textures/bomb");
            Slot.texture = Content.Load<Texture2D>("Textures/powerups");
            mousePointer = Content.Load<Texture2D>("Textures/cursor");

            Bomb.explosion1 = Content.Load<Texture2D>("Textures/explosion1");
            Bomb.explosion2 = Content.Load<Texture2D>("Textures/explosion2");
            Bomb.explosion3 = Content.Load<Texture2D>("Textures/explosion3");
            Bomb.explosion4 = Content.Load<Texture2D>("Textures/explosion4");

            Bomb.explosion = Content.Load<SoundEffect>("Sound/explosion");

            Song song = Content.Load<Song>("Sound/GameSound");

            Lake.texture = Content.Load<Texture2D>("Textures/Lakee");
            

            font = Content.Load<SpriteFont>("Fonts/font");
            Button.spriteBatch = spriteBatch;
            Button.game = this;
            Menu.game = this;
            MenuManager.game = this;

            menu = new MenuManager();

            play = new Button(() =>
            {
                Initialize(); 
                game_state = 2;
                MediaPlayer.IsRepeating = true;
                MediaPlayer.Play(song);
            },
                new Vector2(viewport.Width/2 - 200, viewport.Height/2 - 75),
                new Vector2(viewport.Width/2 - 200, viewport.Height/2 - 75),
                Content.Load<Texture2D>("playagain1"),
                Content.Load<Texture2D>("playagain2"));
            exit = new Button(() => { Exit(); },
                new Vector2(viewport.Width/2 - 200, viewport.Height/2 + 125),
                new Vector2(viewport.Width/2 - 200, viewport.Height/2 + 125),
                Content.Load<Texture2D>("exit"),
                Content.Load<Texture2D>("exit2"));

            Bomb.game = this;
            Bomb.cut_manager = cut_manager;
            Hole.cut_manager = cut_manager;
        }

        protected override void Update(GameTime gameTime)
        {

            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();

            // TODO: Add your update logic here
            UpdateMouse();
            if (game_state == 1)
            {
                menu.Update();

                return;
            }
            if (player1.alive == 0 || player2.alive == 0)
            {
                play.Update();
                exit.Update();
                if (Keyboard.GetState().IsKeyDown(Keys.Tab))
                {
                    this.Initialize();
                }
                return;
            }
            player1.Update(gameTime, player2, bomb1,slot1,powerup);
            player2.Update(gameTime, player1, bomb2,slot2,powerup);
            cut_manager.Update();
            powerup.Update(gameTime, player1, player2);
            bomb1.Update(gameTime, player1);
            bomb2.Update(gameTime, player2);
            slot1.Update(gameTime, player1);
            slot2.Update(gameTime, player2);

            if (player1.Check_Collision_Player(player2))
                player1.Collision_Player(player2);

                player1.Get_PowerUp(powerup, player2, slot1);
                player2.Get_PowerUp(powerup, player1, slot2);
            base.Update(gameTime);

            //System.Diagnostics.Debug.Write(slot1.Slot1, "\n");
            //System.Diagnostics.Debug.Write(slot2.Slot2, "\n");
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color (79, 129, 189));
            spriteBatch.Begin();
            if (game_state == 1)
            {
                menu.Draw();
            }
            else
            {
                frozen_lake.Draw();
                if (player1.alive == 0 || player2.alive == 0)
                {
                    String draw = "It's a draw!", blue = "Player Blue wins!!!", orange = "Player Orange wins!!!";
                    float drawSize = font.MeasureString(draw).X, blueSize = font.MeasureString(blue).X, orangeSize = font.MeasureString(orange).X;
                    if (player1.alive == 0 && player2.alive == 0)
                        spriteBatch.DrawString(font, draw, new Vector2(ScreenWidth / 2 - 50 - drawSize / 2, 150), new Color(55, 96, 146));
                    else
                    {
                        if (player1.alive == 1)            
                            spriteBatch.DrawString(font, blue, new Vector2(ScreenWidth/2 - 50 - blueSize/2, 150), new Color(55, 96, 146));  
                        if (player2.alive == 1)
                            spriteBatch.DrawString(font, orange, new Vector2(ScreenWidth / 2 - 50 - orangeSize / 2, 150), new Color(55, 96, 146));
                    }
                }
                cut_manager.Draw();
                bomb1.Draw(player1);
                bomb2.Draw(player2);
                powerup.Draw();
                player1.Draw(powerup);
                player2.Draw(powerup);
                slot1.Draw();
                slot2.Draw();
            }
            if (player1.alive == 0 || player2.alive == 0)
            {
                play.Draw();
                exit.Draw();
            }
            spriteBatch.Draw(mousePointer, cursorPos, Color.White);
            base.Draw(gameTime);
            spriteBatch.End();
        }

        #region Mouse
        protected void UpdateMouse()
        {
            MouseState current_mouse = Mouse.GetState();

            // The mouse x and y positions are returned relative to the
            // upper-left corner of the game window.

            cursorPos.X= current_mouse.X;
            cursorPos.Y = current_mouse.Y;
        }
        #endregion

    }  
}
