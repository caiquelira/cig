﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Jogo
{
    class CutManager
    {
        public Game1 game;
        SpriteBatch spriteBatch;
        List<Cut> cuts;
        public int[] field;
        HoleManager hole_manager;
        public CutManager(Game1 g)
        {
            hole_manager = new HoleManager(this);
            game = g;
            cuts = new List<Cut>();
            field = new int[600000];
            // Preciso usar field como um array [1000][600]
        }
        public void Initialize(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
            foreach (Cut c in cuts)
            {
                c.Initialize(spriteBatch);
            }
            hole_manager.Initialize(spriteBatch);
        }
        public int get_field(Vector2 v)
        {
            v.X -= ((int)v.X) % 10;
            v.Y -= ((int)v.Y) % 10;
            return field[(int)v.X * 600 + (int)v.Y];
        }
        public void set_field(Vector2 V, int v)
        {
            V.X -= ((int)V.X) % 10;
            V.Y -= ((int)V.Y) % 10;
            field[(int)(V.X) * 600 + (int)(V.Y)] = v;
        }
        public void Add(Cut c)
        {
            cuts.Add(c);
            c.Initialize(spriteBatch); 
            this.set_field(c.pos, 1);
        }
        public void Update()
        {
            for (int i = 0; i < cuts.Count; i++)
                if (cuts[i].alive == 0)
                {
                    cuts.RemoveAt(i);
                    i--;
                    // Possível fontes de BUGS, eu não sei o que acontece quando eu removo
                }
            foreach (Cut c in cuts)
            {
                c.Update();
            }
            hole_manager.Update();
            List<Vector2> pos = new List<Vector2>();
            pos.Add(game.player1.Position);
            pos.Add(game.player2.Position);

            foreach (Vector2 p in pos)
            {
                /*
                if (game.player1.Position == game.player1.last_position && p == pos[0])
                    continue;
                if (game.player2.Position == game.player2.last_position && p == pos[1])
                    continue;
                */
                if (get_field(p) == 0)
                {
                    set_field(p, 1);
                    this.Add(new Cut(p, this));
                }
                if (get_field(p) == 1)
                {
                    if (hole_manager.makeHole())
                    {
                        if (p == pos[0])
                        {
                            game.player1.Immortal();
                        }
                        else
                        {
                            game.player2.Immortal();
                        }
                    }
                }
                if (get_field(p) == 2)
                {
                    if (p == pos[0])
                        game.player1.Kill();
                    if (p == pos[1])
                        game.player2.Kill();
                    continue;
                }
            }
        }
        public void Draw()
        {
            foreach (Cut c in cuts)
            {
                c.Draw(spriteBatch);
            }
            hole_manager.Draw();
        }
    }
}
