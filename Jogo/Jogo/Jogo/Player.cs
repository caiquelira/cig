﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Jogo
{
    public class Player
    {
        //Atributes
        public Vector2 position, last_position;
        private Vector2 speed_vector;
        private float speed, angle, aceleration;
        public int alive;
        public int immortal;
        private int id; //Nao deve existir

        private static Viewport viewport;
        private static SpriteBatch spriteBatch;
        public Texture2D texture;


        //Properties
        //One of them refers to Position
        public Vector2 Position
        {
            get
            {
                return position;
            }
        }

        //Constructors
        public Player(Vector2 position, float speed, float angle, int id)
        {
            this.position = position;
            this.last_position = position;
            this.speed = speed;
            this.angle = angle;
            this.id = id;
            alive = 1;
        }

        //Methods

        public static void Initialize(Viewport viewport, SpriteBatch spriteBatch)
        {
            Player.viewport = viewport;
            Player.spriteBatch = spriteBatch;
           
        }
        
        public void Load(Texture2D texture)
        {
           // this.texture = texture;

        }
        public void Immortal()
        {
            immortal = 15;
        }

        public void Update(GameTime gameTime)
        {
            last_position = position;
            if (immortal > 0)
                immortal--;
            if (id == 1)
            {
                Vector2 inputDirection = Vector2.Zero;
                if (Keyboard.GetState().IsKeyDown(Keys.Left))
                    angle -= 0.1f;
                if (Keyboard.GetState().IsKeyDown(Keys.Right))
                    angle += 0.1f;
                if (Keyboard.GetState().IsKeyDown(Keys.Up))
                    aceleration = 200;
                if (Keyboard.GetState().IsKeyDown(Keys.Down))
                    aceleration = -200;
            }

            else
            {
                Vector2 inputDirection = Vector2.Zero;
                if (Keyboard.GetState().IsKeyDown(Keys.A))
                    angle -= 0.1f;
                if (Keyboard.GetState().IsKeyDown(Keys.D))
                    angle += 0.1f;
                if (Keyboard.GetState().IsKeyDown(Keys.W))
                    aceleration = 200;
                if (Keyboard.GetState().IsKeyDown(Keys.S))
                    aceleration = -200;
            }
            //Equacao horaria da posicao(vetorial) do MUV
            position += speed_vector*(float)gameTime.ElapsedGameTime.TotalSeconds + aceleration * new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * 
                (float)gameTime.ElapsedGameTime.TotalSeconds * (float)gameTime.ElapsedGameTime.TotalSeconds;

            //Equacao horaria da velocidade(vetorial) do MUV
            speed_vector += aceleration * new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) *(float)gameTime.ElapsedGameTime.TotalSeconds;
            
            //Mudar: criar classe para herdar as outras **************
            if (id ==1) {
            //Desaceleracao do Player quando nao houver input do usuario
                if (speed_vector != new Vector2(0, 0) && !Keyboard.GetState().IsKeyDown(Keys.Up) && !Keyboard.GetState().IsKeyDown(Keys.Down))
                {
                    speed_vector *= (float)0.975;
                }
                else
                    aceleration = 0;
            }

            else {
                 if (speed_vector != new Vector2(0, 0) && !Keyboard.GetState().IsKeyDown(Keys.W) && !Keyboard.GetState().IsKeyDown(Keys.S))
                {
                speed_vector *= (float)0.975;
                }

                else
                aceleration = 0;
            }
            //Condicao para haver colisao com a parede
            if (Check_Collision_Wall())
                Collision_Wall();

        } 


        public void Draw()
        {
            spriteBatch.Draw(
                texture,     
                position,    
                null,              
                Color.White,    
                angle,      
                new Vector2(texture.Width / 2, texture.Height / 2),
                1,                 
                SpriteEffects.None, 
                0.0f);              
        }

        public bool Check_Collision_Player(Player player)
        {

            if (texture.Width * texture.Width <(player.position.X - this.position.X) * (player.position.X - this.position.X) +
                (player.position.Y - this.position.Y) * (player.position.Y - this.position.Y))
                return false;
            else 
                return true;

        }

        public bool Check_Collision_Wall()
        {
            if (position.X < texture.Width / 2) return true;
            if (position.Y < texture.Width / 2) return true;
            if (position.X > 1000 - texture.Width / 2) return true;
            if (position.Y > 600 - texture.Width / 2) return true;

            return false;
        }

        //Concertar as colisoes bidimensionais depois*******************
        public void Collision_Player(Player player)
        {
            if (Check_Collision_Player(player))
            {
                //Vector na direcao da colisao
                Vector2 direction = new Vector2(player.position.X - position.X, player.position.Y - position.Y);

                float angle = -(float)Math.Atan(player.position.Y - position.Y / player.position.X - position.X);

                speed_vector = RotateVector2(speed_vector, angle);
                player.speed_vector = RotateVector2(player.speed_vector, angle);


                float temp = speed_vector.X;
                speed_vector.X = player.speed_vector.X;
                player.speed_vector.X = temp;


                speed_vector = RotateVector2(speed_vector, -angle);
                player.speed_vector = RotateVector2(player.speed_vector, -angle);

                speed_vector += -direction;
                player.speed_vector += direction;
            }
        }

        public static Vector2 RotateVector2(Vector2 point, float radians)
        {
            float cosRadians = (float)Math.Cos(radians);
            float sinRadians = (float)Math.Sin(radians);

            return new Vector2(
                point.X * cosRadians - point.Y * sinRadians,
                point.X * sinRadians + point.Y * cosRadians);
        }

        public void Collision_Wall()
        {
            //coeficiente de restituicao
            float e = 0.5f;
            if (position.X < texture.Width / 2)
                 speed_vector.X = Math.Abs(speed_vector.X);
            if (position.Y < texture.Width / 2)
                speed_vector.Y = Math.Abs(speed_vector.Y);
            if (position.X > 1000 - texture.Width / 2)
                speed_vector.X = -Math.Abs(speed_vector.X);
            if (position.Y > 600 - texture.Width / 2)
                speed_vector.Y = -Math.Abs(speed_vector.Y);

            speed_vector *= (float)e;
        }

        public void Kill()
        {
            if (immortal == 0)
                alive = 0;
        }

        
    }
 
}
