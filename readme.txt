Ice Breakers is a game about breaking ice and making your opponent drown!

As you walk through the ice, you melt it, and if a piece of ice is separated from the rest, it sinks immediately! Use this to your advantage.

Now with many power-ups!
